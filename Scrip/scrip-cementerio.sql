-- MySQL Script generated by MySQL Workbench
-- Mon Nov 12 02:20:04 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Cementerio
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Cementerio
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Cementerio` DEFAULT CHARACTER SET utf8 ;
USE `Cementerio` ;

-- -----------------------------------------------------
-- Table `Cementerio`.`CLIENTE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cementerio`.`CLIENTE` (
  `rut_cliente` INT NOT NULL,
  `nombre_cliente` VARCHAR(45) NULL,
  `apellido_p` VARCHAR(45) NULL,
  `apellido_m` VARCHAR(45) NULL,
  `direccion` VARCHAR(45) NULL,
  `telefono` INT NULL,
  PRIMARY KEY (`rut_cliente`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cementerio`.`FUNCIONARIO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cementerio`.`FUNCIONARIO` (
  `rut_funcionario` INT NOT NULL,
  `nombre_funcionario` VARCHAR(45) NULL,
  `apellido_pf` VARCHAR(45) NULL,
  `apellido_mf` VARCHAR(45) NULL,
  PRIMARY KEY (`rut_funcionario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cementerio`.`PRODUCTO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cementerio`.`PRODUCTO` (
  `cod_producto` INT NOT NULL,
  `nombre_producto` VARCHAR(45) NULL,
  `ubicacion` VARCHAR(45) NULL,
  `estado` VARCHAR(45) NULL,
  PRIMARY KEY (`cod_producto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cementerio`.`FECHA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cementerio`.`FECHA` (
  `idFECHA` DATETIME NOT NULL,
  PRIMARY KEY (`idFECHA`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cementerio`.`CONTRATO-VENTA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cementerio`.`CONTRATO-VENTA` (
  `id_contrato` INT NOT NULL,
  `precio` VARCHAR(45) NULL,
  `PRODUCTO_cod_producto` INT NOT NULL,
  `FECHA_idFECHA` DATETIME NOT NULL,
  `CLIENTE_rut_cliente` INT NOT NULL,
  `FUNCIONARIO_rut_funcionario` INT NOT NULL,
  PRIMARY KEY (`id_contrato`, `PRODUCTO_cod_producto`, `FECHA_idFECHA`),
  INDEX `fk_CONTRATO-VENTA_PRODUCTO_idx` (`PRODUCTO_cod_producto` ASC) VISIBLE,
  INDEX `fk_CONTRATO-VENTA_FECHA1_idx` (`FECHA_idFECHA` ASC) VISIBLE,
  INDEX `fk_CONTRATO-VENTA_CLIENTE1_idx` (`CLIENTE_rut_cliente` ASC) VISIBLE,
  INDEX `fk_CONTRATO-VENTA_FUNCIONARIO1_idx` (`FUNCIONARIO_rut_funcionario` ASC) VISIBLE,
  CONSTRAINT `fk_CONTRATO-VENTA_PRODUCTO`
    FOREIGN KEY (`PRODUCTO_cod_producto`)
    REFERENCES `Cementerio`.`PRODUCTO` (`cod_producto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CONTRATO-VENTA_FECHA1`
    FOREIGN KEY (`FECHA_idFECHA`)
    REFERENCES `Cementerio`.`FECHA` (`idFECHA`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CONTRATO-VENTA_CLIENTE1`
    FOREIGN KEY (`CLIENTE_rut_cliente`)
    REFERENCES `Cementerio`.`CLIENTE` (`rut_cliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CONTRATO-VENTA_FUNCIONARIO1`
    FOREIGN KEY (`FUNCIONARIO_rut_funcionario`)
    REFERENCES `Cementerio`.`FUNCIONARIO` (`rut_funcionario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
