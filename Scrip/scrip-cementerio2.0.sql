-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-11-2018 a las 03:17:02
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cementerio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `CLIENTE` (
  `rut_cliente` varchar(10) CHARACTER SET utf8 NOT NULL,
  `nombre_cliente` varchar(20) CHARACTER SET utf8 NOT NULL,
  `apellido_m` varchar(20) CHARACTER SET utf8 NOT NULL,
  `apellido_p` varchar(20) CHARACTER SET utf8 NOT NULL,
  `direccion` varchar(30) CHARACTER SET utf8 NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratoventa`
--

CREATE TABLE `CONTRATOVENTA` (
  `rut_cliente` varchar(10) CHARACTER SET utf8 NOT NULL,
  `fecha_contrato` date NOT NULL,
  `rut_funcionario` varchar(10) CHARACTER SET utf8 NOT NULL,
  `cod_producto` int(11) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionario`
--

CREATE TABLE `FUNCIONARIO` (
  `rut_funcionario` varchar(10) CHARACTER SET utf8 NOT NULL,
  `nombre_funcionario` varchar(20) CHARACTER SET utf8 NOT NULL,
  `apellido_mf` varchar(20) CHARACTER SET utf8 NOT NULL,
  `apellido_pf` varchar(20) CHARACTER SET utf8 NOT NULL,
  `prevision` varchar(20) CHARACTER SET utf8 NOT NULL,
  `afp` varchar(20) CHARACTER SET utf8 NOT NULL,
  `sueldo_base`  int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mantencion`
--

CREATE TABLE `MANTENCION` (
  `fecha_mantencion` date NOT NULL,
  `cod_producto` int(11) NOT NULL,
  `rut_funcionario` varchar(10) CHARACTER SET utf8 NOT NULL,
  `tipo_mantencion` varchar(30) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `PRODUCTO` (
  `cod_producto` int(11) NOT NULL,
  `nombre_producto` varchar(20) NOT NULL,
  `ubicacion` varchar(30) NOT NULL,
  `estado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `CLIENTE`
  ADD PRIMARY KEY (`rut_cliente`);

--
-- Indices de la tabla `contratoventa`
--
ALTER TABLE `CONTRATOVENTA`
  ADD PRIMARY KEY (`rut_cliente`,`fecha_contrato`,`rut_funcionario`,`cod_producto`),
  ADD KEY `cod_producto` (`cod_producto`),
  ADD KEY `rut_funcionario` (`rut_funcionario`);

--
-- Indices de la tabla `funcionario`
--
ALTER TABLE `FUNCIONARIO`
  ADD PRIMARY KEY (`rut_funcionario`);

--
-- Indices de la tabla `mantencion`
--
ALTER TABLE `MANTENCION`
  ADD PRIMARY KEY (`fecha_mantencion`,`cod_producto`,`rut_funcionario`),
  ADD KEY `rut_funcionario` (`rut_funcionario`),
  ADD KEY `cod_producto` (`cod_producto`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `PRODUCTO`
  ADD PRIMARY KEY (`cod_producto`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `contratoventa`
--
ALTER TABLE `CONTRATOVENTA`
  ADD CONSTRAINT `contratoventa_ibfk_2` FOREIGN KEY (`cod_producto`) REFERENCES `PRODUCTO` (`cod_producto`) ON UPDATE CASCADE,
  ADD CONSTRAINT `contratoventa_ibfk_3` FOREIGN KEY (`rut_funcionario`) REFERENCES `FUNCIONARIO` (`rut_funcionario`) ON UPDATE CASCADE,
  ADD CONSTRAINT `contratoventa_ibfk_4` FOREIGN KEY (`rut_cliente`) REFERENCES `CLIENTE` (`rut_cliente`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `mantencion`
--
ALTER TABLE `MANTENCION`
  ADD CONSTRAINT `mantencion_ibfk_1` FOREIGN KEY (`rut_funcionario`) REFERENCES `FUNCIONARIO` (`rut_funcionario`) ON UPDATE CASCADE,
  ADD CONSTRAINT `mantencion_ibfk_2` FOREIGN KEY (`cod_producto`) REFERENCES `PRODUCTO` (`cod_producto`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
