/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appcementerio.Modelo;

import appcementerio.Database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author exsabgo
 */
public class Producto {
    private int cod_producto;
    private String nombre_producto;
    private String ubicacion;
    private String estado;
    
public void Producto() {
    }

    public void crearProducto(int cod_producto, String nombre_producto, String ubicacion, String estado) {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "insert into PRODUCTO(cod_producto,nombre_producto,ubicacion,estado) values ('";
        sql += cod_producto + "','" + nombre_producto + "','" + ubicacion + "','" + estado + "')";

        try {
            st = (Statement) con.createStatement();
            st.execute(sql);
            con.close();
            System.out.println("Producto creado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }

    }

    public void actualizarProducto(int cod_producto, String nombre_producto, String ubicacion, String estado) throws SQLException {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();

        PreparedStatement ps = (PreparedStatement) con.prepareStatement(""
                + "update CLIENTE set nombre_producto = ?, ubicacion = ?,estado = ?"
                + " WHERE cod_producto = ? ");

        try {
            ps.setString(1, nombre_producto);
            ps.setString(2, ubicacion);
            ps.setString(3, estado);
            ps.setInt(7, cod_producto);
            ps.executeUpdate();
            ps.close();
            con.close();
            System.out.println("Producto Actualizado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }
    }

    public void eliminarProducto(int id) {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "delete from CLIENTE where cod_producto=" + id;

        try {
            st = (Statement) con.createStatement();
            st.execute(sql);
            con.close();
            System.out.println("Producto eliminado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }
    }

    public ArrayList<Producto> obtenerProductos() {

        ArrayList<Producto> listaProducto = new ArrayList<Producto>();
        String sql = "";
        ResultSet rs = null;
        PreparedStatement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "select * from  CLIENTE";
        System.out.println("consulta: " + sql);
        try {
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Producto objProducto = new Producto();
                objProducto.setId(rs.getInt("cod_producto"));
                objProducto.setNombreP(rs.getString("nombre_producto"));
                objProducto.setUbicacion(rs.getString("ubicacion"));
                objProducto.setEstado(rs.getString("estado"));

                listaProducto.add(objProducto);
                objProducto = null;
            }
            con.close();
        } catch (SQLException e) {
            System.err.println("Error " + e.getMessage());
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                st = null;
                rs = null;
                con.close();
            } catch (SQLException ee) {
                System.err.println("Error " + ee.getMessage());
            }

        }

        return listaProducto;
    }

    //Getter Y setter
    public Integer getId() {
        return cod_producto;
    }

    public void setId(Integer id) {
        this.cod_producto = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getAUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getNombre() {
        return nombre_producto;
    }

    public void setNombreP(String nombre) {
        this.nombre_producto = nombre;
    }



}
