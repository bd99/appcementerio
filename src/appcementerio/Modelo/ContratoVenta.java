/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appcementerio.Modelo;

import appcementerio.Database.Database;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Seba
 */
public class ContratoVenta {

    private String rut_cliente;
    private String fecha_contrato;
    private String rut_funcionario;
    private int cod_producto;
    private int precio;

    public void ContratoVenta() {
    }

    public void crearProducto(String rut_cliente, String fecha_contrato, String rut_funcionario, String cod_producto, int precio) {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "insert into PRODUCTO(rut_cliente,fecha_contrato,rut_funcionario,cod_producto) values ('";
        sql += rut_cliente + "','" + fecha_contrato + "','" + rut_funcionario + "','" + cod_producto + "','" + precio + "')";

        try {
            st = (Statement) con.createStatement();
            st.execute(sql);
            con.close();
            System.out.println("Contrato Venta creado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }

    }

    public void actualizarContratoVenta(String rut_cliente, String fecha_contrato, String rut_funcionario, int cod_producto, int precio) throws SQLException {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();

        PreparedStatement ps = (PreparedStatement) con.prepareStatement(""
                + "update CLIENTE set fecha_contrato = ?, rut_funcionario = ?,cod_producto = ? ,precio = ?"
                + " WHERE rut_cliente = ? ");

        try {
            ps.setString(1, fecha_contrato);
            ps.setString(2, rut_funcionario);
            ps.setString(3, rut_cliente);
            ps.setInt(4, cod_producto);
            ps.setInt(5, precio);
            ps.executeUpdate();
            ps.close();
            con.close();
            System.out.println("ContratoVenta Actualizado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }
    }

    public void eliminarContratoVenta(int id) {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "delete from CLIENTE where rut_cliente=" + id;

        try {
            st = (Statement) con.createStatement();
            st.execute(sql);
            con.close();
            System.out.println("ContratoVenta eliminado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }
    }

    public ArrayList<ContratoVenta> obtenerCventa() {

        ArrayList<ContratoVenta> listaContratoVenta = new ArrayList<ContratoVenta>();
        String sql = "";
        ResultSet rs = null;
        PreparedStatement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "select * from  CLIENTE";
        System.out.println("consulta: " + sql);
        try {
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                ContratoVenta objContratoVenta = new ContratoVenta();
                objContratoVenta.setId(rs.getString("rut_cliente"));
                objContratoVenta.setFechaContrato(rs.getString("fecha_contrato"));
                objContratoVenta.setRutF(rs.getString("rut_funcionario"));
                objContratoVenta.setCproducto(rs.getInt("cod_producto"));

                listaContratoVenta.add(objContratoVenta);
                objContratoVenta = null;
            }
            con.close();
        } catch (SQLException e) {
            System.err.println("Error " + e.getMessage());
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                st = null;
                rs = null;
                con.close();
            } catch (SQLException ee) {
                System.err.println("Error " + ee.getMessage());
            }

        }

        return listaContratoVenta;
    }

    //Getter Y setter
    public String getId() {
        return rut_cliente;
    }

    public void setId(String id) {
        this.rut_cliente = id;
    }

    public int getCproducto() {
        return cod_producto;
    }

    public void setCproducto(int cp) {
        this.cod_producto = cp;
    }

    public String getRutF() {
        return rut_funcionario;
    }

    public void setRutF(String rutF) {
        this.rut_funcionario = rutF;
    }

    public String getFechaContrato() {
        return fecha_contrato;
    }

    public void setFechaContrato(String nombre) {
        this.fecha_contrato = nombre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int cp) {
        this.precio = cp;
    }

}
