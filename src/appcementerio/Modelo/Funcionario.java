/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appcementerio.Modelo;

import appcementerio.Database.Database;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author exsabgo
 */
public class Funcionario {

    private int rut_funcionario;
    private String nombre_funcionario;
    private String apellido_pf;
    private String apellido_mf;
    private String prevision;
    private String afp;
    private int sueldo_base;

    public void Funcionario() {
    }

    public void crearFuncionario(int rut_funcionario, String nombre_funcionario, String apellido_pf, String apellido_mf, String prevision, String afp, int sueldo_base) {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "insert into FUNCIONARIO(rut_funcionario,nombre_funcionario,apellido_pf,apellido_mf,prevision,afp,sueldo_base) values ('";
        sql += rut_funcionario + "','" + nombre_funcionario + "','" + apellido_pf + "','" + apellido_mf  + "','"  + prevision + "','" + afp + "','" + sueldo_base + "')";

        try {
            st = (Statement) con.createStatement();
            st.execute(sql);
            con.close();
            System.out.println("Funcionario creado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }

    }

    public void actualizarFuncionario(int rut_funcionario, String nombre_funcionario, String apellido_pf, String apellido_mf, String prevision, String afp, int sueldo_base) throws SQLException {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();

        PreparedStatement ps = (PreparedStatement) con.prepareStatement(""
                + "update CLIENTE set nombre_funcionario = ?, apellido_pf = ?,apellido_mf = ?,prevision = ?,afp = ?,sueldo_base = ?"
                + " WHERE rut_funcionario = ? ");

        try {
            ps.setString(1, nombre_funcionario);
            ps.setString(2, apellido_pf);
            ps.setString(3, apellido_mf);
            ps.setString(4, prevision);
            ps.setString(5, afp);
            ps.setInt(6, sueldo_base);
            ps.setInt(7, rut_funcionario);
            ps.executeUpdate();
            ps.close();
            con.close();
            System.out.println("Funcionario Actualizado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }
    }

    public void eliminarFuncionario(int id) {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "delete from CLIENTE where rut_funcionario=" + id;

        try {
            st = (Statement) con.createStatement();
            st.execute(sql);
            con.close();
            System.out.println("Funcionario eliminado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }
    }

    public ArrayList<Funcionario> obtenerPersonas() {

        ArrayList<Funcionario> listaFuncionario = new ArrayList<Funcionario>();
        String sql = "";
        ResultSet rs = null;
        PreparedStatement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "select * from  CLIENTE";
        System.out.println("consulta: " + sql);
        try {
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Funcionario objFuncionario = new Funcionario();
                objFuncionario.setId(rs.getInt("rut_funcionario"));
                objFuncionario.setNombre(rs.getString("nombre_funcionario"));
                objFuncionario.setApellidoP(rs.getString("apellido_pf"));
                objFuncionario.setApellidoM(rs.getString("apellido_mf"));
                objFuncionario.setPrevision(rs.getString("prevision"));
                objFuncionario.setAfp(rs.getString("afp"));
                objFuncionario.setSueldoBase(rs.getInt("sueldo_base"));

                listaFuncionario.add(objFuncionario);
                objFuncionario = null;
            }
            con.close();
        } catch (SQLException e) {
            System.err.println("Error " + e.getMessage());
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                st = null;
                rs = null;
                con.close();
            } catch (SQLException ee) {
                System.err.println("Error " + ee.getMessage());
            }

        }

        return listaFuncionario;
    }

    //Getter Y setter
    public Integer getId() {
        return rut_funcionario;
    }

    public void setId(Integer id) {
        this.rut_funcionario = id;
    }

    public String getApellidoM() {
        return apellido_mf;
    }

    public void setApellidoM(String apellidoM) {
        this.apellido_mf = apellidoM;
    }

    public String getApellidoP() {
        return apellido_pf;
    }

    public void setApellidoP(String apellidoP) {
        this.apellido_pf = apellidoP;
    }

    public String getNombre() {
        return nombre_funcionario;
    }

    public void setNombre(String nombre) {
        this.nombre_funcionario = nombre;
    }
    
    
    
    
    public String getPrevision() {
        return prevision;
    }

    public void setPrevision(String nombre) {
        this.prevision = nombre;
    }
    
    public String getAfp() {
        return afp;
    }

    public void setAfp(String nombre) {
        this.afp = nombre;
    }
    
        public int getSueldoBase() {
        return sueldo_base;
    }

    public void setSueldoBase(int nombre) {
        this.sueldo_base = nombre;
    }

}
