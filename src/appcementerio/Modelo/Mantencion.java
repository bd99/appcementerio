/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appcementerio.Modelo;
import appcementerio.Database.Database;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Seba
 */
public class Mantencion {
    
private String fecha_mantencion;
    private int cod_producto;
    private String rut_funcionario;
    private String tipo_mantencion;
    
public void Mantencion() {
    }

    public void crearProducto(String fecha_mantencion, int cod_producto, String rut_funcionario, String tipo_mantencion) {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "insert into MANTENCION(fecha_mantencion,cod_producto,rut_funcionario,tipo_mantencion) values ('";
        sql += fecha_mantencion + "','" + cod_producto + "','" + rut_funcionario + "','" + tipo_mantencion + "')";

        try {
            st = (Statement) con.createStatement();
            st.execute(sql);
            con.close();
            System.out.println("Producto creado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }

    }

    public void actualizarMantencion(String fecha_mantencion, int cod_producto, String rut_funcionario, String tipo_mantencion) throws SQLException {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();

        PreparedStatement ps = (PreparedStatement) con.prepareStatement(""
                + "update CLIENTE set cod_producto = ?, rut_funcionario = ?,tipo_mantencion = ?"
                + " WHERE fecha_mantencion = ? ");

        try {
            ps.setInt(1, cod_producto);
            ps.setString(2, rut_funcionario);
            ps.setString(3, tipo_mantencion);
            ps.setString(7, fecha_mantencion);
            ps.executeUpdate();
            ps.close();
            con.close();
            System.out.println("Mantencion Actualizado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }
    }

    public void eliminarMantencion(int id) {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "delete from CLIENTE where fecha_mantencion=" + id;

        try {
            st = (Statement) con.createStatement();
            st.execute(sql);
            con.close();
            System.out.println("Mantencion eliminada");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }
    }

    public ArrayList<Mantencion> obtenerMantencion() {

        ArrayList<Mantencion> listaMantencion = new ArrayList<Mantencion>();
        String sql = "";
        ResultSet rs = null;
        PreparedStatement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "select * from  CLIENTE";
        System.out.println("consulta: " + sql);
        try {
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Mantencion objMantencion = new Mantencion();
                objMantencion.setId(rs.getString("fecha_mantencion"));
                objMantencion.setCod_prod(rs.getInt("cod_producto"));
                objMantencion.getRutF(rs.getString("rut_funcionario"));
                objMantencion.setTipoMantencion(rs.getString("tipo_mantencion"));

                listaMantencion.add(objMantencion);
                objMantencion = null;
            }
            con.close();
        } catch (SQLException e) {
            System.err.println("Error " + e.getMessage());
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                st = null;
                rs = null;
                con.close();
            } catch (SQLException ee) {
                System.err.println("Error " + ee.getMessage());
            }

        }

        return listaMantencion;
    }

    //Getter Y setter
    public String getId() {
        return fecha_mantencion;
    }

    public void setId(String id) {
        this.fecha_mantencion = id;
    }

    public String getTipoMantencion() {
        return tipo_mantencion;
    }

    public void setTipoMantencion(String tm) {
        this.tipo_mantencion = tm;
    }

    public String getRutF() {
        return rut_funcionario;
    }

    public void getRutF(String rutF) {
        this.rut_funcionario = rutF;
    }

    public int getCod_prod() {
        return cod_producto;
    }

    public void setCod_prod(int nombre) {
        this.cod_producto = nombre;
    }



}
