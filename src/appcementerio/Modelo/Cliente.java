/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appcementerio.Modelo;

import appcementerio.Database.Database;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Seba
 */
public class Cliente {

    private int rut_cliente;
    private String nombre_cliente;
    private String apellido_p;
    private String apellido_m;
    private String direccion;
    private int telefono;

    public void cliente() {
    }

    public void crearCliente(int rut_cliente, String nombre_cliente, String apellido_p, String apellido_m, String direccion, int telefono) {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "insert into CLIENTE(rut_cliente,nombre_cliente,apellido_p,apellido_m,direccion,telefono) values ('";
        sql += rut_cliente + "','" + nombre_cliente + "','" + apellido_p + "','" + apellido_m + "','" + direccion + "','" + telefono + "')";

        try {
            st = (Statement) con.createStatement();
            st.execute(sql);
            con.close();
            System.out.println("Cliente creado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }

    }

    public void actualizarCliente(int rut_cliente, String nombre_cliente, String apellido_p, String apellido_m, String direccion, int telefono) throws SQLException {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();

        PreparedStatement ps = (PreparedStatement) con.prepareStatement(""
                + "update CLIENTE set nombre_cliente = ?, apellido_p = ?,apellido_m = ?,direccion = ?,telefono = ?"
                + " WHERE rut_cliente = ? ");

        try {
            ps.setString(1, nombre_cliente);
            ps.setString(2, apellido_p);
            ps.setString(3, apellido_m);
            ps.setString(4, direccion);
            ps.setInt(5, telefono);
            ps.setInt(6, rut_cliente);
            ps.executeUpdate();
            ps.close();
            con.close();
            System.out.println("Cliente Actualizado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }
    }

    public void eliminarCliente(int id) {
        String sql = "";
        Statement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "delete from CLIENTE where rut_cliente=" + id;

        try {
            st = (Statement) con.createStatement();
            st.execute(sql);
            con.close();
            System.out.println("Cliente eliminado");
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }
    }

    public ArrayList<Cliente> buscarCliente() {

        ArrayList<Cliente> listaCliente = new ArrayList<Cliente>();
        String sql = "";
        ResultSet rs = null;
        PreparedStatement st = null;
        Connection con = null;
        Database database = new Database("192.168.99.100:32769", "user", "pass", "Cementerio");
        con = database.conectar();
        sql = "select * from  CLIENTE";
        System.out.println("consulta: " + sql);
        try {
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Cliente objCliente = new Cliente();
                objCliente.setId(rs.getInt("rut_cliente"));
                objCliente.setNombre(rs.getString("nombre_cliente"));
                objCliente.setApellidoP(rs.getString("apellido_p"));
                objCliente.setApellidoM(rs.getString("apellido_m"));
                objCliente.setDireccion(rs.getString("Direccion"));
                objCliente.setTelefono(rs.getInt("telefono"));

                listaCliente.add(objCliente);
                objCliente = null;
            }
            con.close();
        } catch (SQLException e) {
            System.err.println("Error " + e.getMessage());
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
                st = null;
                rs = null;
                con.close();
            } catch (SQLException ee) {
                System.err.println("Error " + ee.getMessage());
            }

        }

        return listaCliente;
    }

    //Getter Y setter
    public Integer getId() {
        return rut_cliente;
    }

    public void setId(Integer id) {
        this.rut_cliente = id;
    }

    public String getApellidoM() {
        return apellido_m;
    }

    public void setApellidoM(String apellidoM) {
        this.apellido_m = apellidoM;
    }

    public String getApellidoP() {
        return apellido_p;
    }

    public void setApellidoP(String apellidoP) {
        this.apellido_p = apellidoP;
    }

    public String getNombre() {
        return nombre_cliente;
    }

    public void setNombre(String nombre) {
        this.nombre_cliente = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String dir) {
        this.direccion = dir;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int fono) {
        this.telefono = fono;
    }

}
